//
//  LoggerFactory.swift
//  CommonLogger
//
//  Created by Bruno Fonseca on 29/12/16.
//  Copyright © 2016 Nosebit. All rights reserved.
//

import Foundation

public enum LoggerLevel : Int {
    case silly = 1, debug, verbose, info, warn, error
}

public class Logger {
    private var factory: LoggerFactory
    private var scope: String
    
    public init(scope: String, factory: LoggerFactory) {
        self.scope = scope
        self.factory = factory
    }
    
    deinit {
        
    }
    
    private func log(level: LoggerLevel, message: String, data: [String:Any]) {
        let namespace = self.factory.namespace
        let scope = self.scope
        let msg = "[\(level)] \(namespace) : \(scope) : \(message)"
        
        if(level.rawValue <= self.factory.level.rawValue) {
            print(msg, data.description)
        }
    }
    
    public func silly(_ message: String, data: [String:Any]) {
        log(level: .silly, message: message, data: data)
    }
    
    public func debug(_ message: String, data: [String:Any]) {
        log(level: .debug, message: message, data: data)
    }
    
    public func verbose(_ message: String, data: [String:Any]) {
        log(level: .verbose, message: message, data: data)
    }
    
    public func info(_ message: String, data: [String:Any]) {
        log(level: .info, message: message, data: data)
    }
    
    public func warn(_ message: String, data: [String:Any]) {
        log(level: .warn, message: message, data: data)
    }
    
    public func error(_ message: String, data: [String:Any]) {
        log(level: .error, message: message, data: data)
    }
}

public class LoggerFactory {
    public private(set) var level: LoggerLevel
    public private(set) var namespace: String
    
    public init(_ namespace: String, level: LoggerLevel = .info) {
        self.namespace = namespace
        self.level = level
    }
    
    deinit {
        
    }
    
    public func test() -> String {
        return namespace
    }
    
    public func create(_ scope: String) -> Logger {
        return Logger(scope: scope, factory: self)
    }
}
