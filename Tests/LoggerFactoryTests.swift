//
//  LoggerFactoryTests.swift
//  CommonLogger
//
//  Created by Bruno Fonseca on 29/12/16.
//  Copyright © 2016 Nosebit. All rights reserved.
//

import XCTest
import CommonLogger

class LoggerFactoryTests: XCTestCase {
    var Logger : LoggerFactory!
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        Logger = LoggerFactory(namespace: "oi")
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        XCTAssert(Logger.test() == "oi")
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
