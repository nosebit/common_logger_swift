# Version 0.0.1 : Released 2016-12-30
INI : Add basic logging functionality and unit tests.
FEA : Add podspec and License.
ENH : Change initial version from 0.0.0 to 0.0.1.
VER : Release version 0.0.1.
