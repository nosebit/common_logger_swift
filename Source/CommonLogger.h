//
//  CommonLogger.h
//  CommonLogger
//
//  Created by Bruno Fonseca on 29/12/16.
//  Copyright © 2016 Nosebit. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for CommonLogger.
FOUNDATION_EXPORT double CommonLoggerVersionNumber;

//! Project version string for CommonLogger.
FOUNDATION_EXPORT const unsigned char CommonLoggerVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <CommonLogger/PublicHeader.h>


